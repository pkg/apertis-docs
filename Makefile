PACKAGE = apertis-docs
REMOTE_PREFIX = https://developer.apertis.org
PDF_OPTIONS ?= --line-numbers

HOTDOC ?= hotdoc
prefix ?= /usr
datadir ?= ${prefix}/share
devhelpdir ?= ${datadir}/devhelp/books
docdir ?= ${datadir}/doc/${PACKAGE}
htmldir ?= ${docdir}

# emulate Automake well enough for hotdoc.mk
AM_V_GEN ?=
AMTAR ?= tar
mkinstalldirs ?= install -d
srcdir = $(CURDIR)
top_srcdir = $(CURDIR)
builddir = $(CURDIR)
top_builddir = $(CURDIR)

HOTDOC_PROJECTS = apertis-docs

apertis_docs_HOTDOC_FLAGS = \
	--conf-file hotdoc.json \
	$(NULL)

all:

install:

clean:

.PHONY: all install clean

-include $(shell $(HOTDOC) --makefile-path)

HTML_TO_PDF = ./hotdoc-html-2-pdf

PDF_FILES = $(sort $(patsubst docs/%.md,build/pdf/%.pdf,$(wildcard docs/*.md)))

build/pdf/%.pdf: $(call HOTDOC_TARGET,apertis-docs) $(HTML_TO_PDF)
	$(HTML_TO_PDF) $(patsubst build/pdf/%.pdf,build/html/%.html,$@) $@ --remote-prefix "${REMOTE_PREFIX}" ${PDF_OPTIONS}

pdf: Makefile $(PDF_FILES)

clean-pdf:
	rm -rf build/pdf

clean: clean-pdf

GITIGNOREFILES += \
	media/\*.svg.pdf \
	$(NULL)

-include git.mk
