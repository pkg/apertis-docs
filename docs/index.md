---
short-description: "Apertis application developer portal"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Apertis application developer guide

Welcome to the Apertis application developer guide!

The [FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software) ([Free](http://en.wikipedia.org/wiki/Free_software) and [open source](http://en.wikipedia.org/wiki/Open-source_software)) GNU/Linux-based Apertis platform provides a set of APIs and services that allow you to create automotive information and entertainment apps.  These APIs and services are made available in the Apertis SDK.

The SDK is a linux virtual machine running a standard desktop environment on top of the Apertis distribution.  It contains Apertis development libraries, developer tools, documentation, debugging libraries and the Apertis simulator.  The SDK VM image can be run on Windows, Mac OS, or different Linux distributions using the virtualization software [VirtualBox](vm-setup.md).

### Next

|-|
| ![](media/continue-arrow.png) **Questions?** Read the [FAQ](faq.md). |
| ![](media/continue-arrow.png) **Ready to start development?** [Install the SDK](install.md). |
| ![](media/continue-arrow.png) **Want to create or update Apertis apps?**  Find out [What's new in the latest version of Apertis](#whats-new-in-this-version) or use the [API quick links](#apertis-api-quick-links) below. |
| ![](media/continue-arrow.png) **Ready to distribute your app?**  Continue on to [Distribution](app-distribution.md). |


## Support

If you encounter any problems using the SDK and need help, please send us an email at <sdk-support@apertis.org>.

## Apertis API Quick Links
### Platform libraries
[Traprain](apis.md#traprain): navigation and routing libraries
[libclapton](apis.md#libclapton): system information and logging library  
[libgrassmoor](apis.md#libgrassmoor): media information and playback library  
[liblightwood](apis.md#liblightwood): widget library  
[Mildenhall](apis.md#mildenhall): user interface widget library  
[libseaton](apis.md#libseaton): persistent data management library  
[libthornbury](apis.md#libthornbury): UI utility library  

### Platform user-services
[Barkway](apis.md#barkway): global popup management framework
[Canterbury](apis.md#canterbury): application management and process control service  
[Didcot](apis.md#didcot): data sharing and file opening service  
[Newport](apis.md#newport): download manager  
[Prestwood](apis.md#prestwood): disk mounting service  
[Tinwell](apis.md#tinwell): media playback service  
[Ribchester](apis.md#ribchester): Application installer and mounting service  
[Rhosydd](apis.md#rhosydd): service for handling access to sensors and actuators

## Upstream APIs
### Enabling APIs

#### Content Rendering
[WebKitGTK+ Clutter](https://gitlab.apertis.org/hmi/webkit-gtk-clutter/): Web engine  
[Poppler](https://poppler.freedesktop.org/): PDF rendering  
[ClutterGst 3](http://developer.gnome.org/clutter-gst/unstable/): High-level multimedia  

#### UI
[Cairo](http://cairographics.org/documentation/): Drawing library  
[Clutter](https://developer.gnome.org/clutter/stable/): High-level graphics  
[Mx 2.0](https://github.com/clutter-project/mx): UI toolkit  

#### Services
[Canberra](http://0pointer.de/lennart/projects/libcanberra/gtkdoc/): High-level sounds  
[PolicyKit](https://www.freedesktop.org/software/polkit/docs/latest/polkit.8.html): System  
[Folks](http://telepathy.freedesktop.org/doc/folks/c/): Contacts & calendar  
[Grilo](http://developer.gnome.org/grilo/0.2/): Media indexing  
[Tracker-extract](https://developer.gnome.org/libtracker-extract/0.16/), [Tracker-miner](https://developer.gnome.org/libtracker-miner/0.16/), [Tracker-sparql](https://developer.gnome.org/libtracker-sparql/0.16/): Media indexing  
[libsecret](https://developer.gnome.org/libsecret/0.16/): Secrets management  
[Telepathy](http://telepathy.freedesktop.org/doc/book/index.html): Communication  
[ofono](https://01.org/ofono): Communication  
[Bluez](http://www.bluez.org/): Bluetooth service  

### OS APIs
#### Infrastructure
[eglibc](http://www.gnu.org/software/libc/manual/html_node/index.html): Base  
[GLib](https://developer.gnome.org/glib/stable/): Base  

#### Rendering building blocks
[pixman](http://www.pixman.org/): Low-level graphics  
[Pango](https://developer.gnome.org/pango/stable/): High-level font rendering  
[Cogl](https://www.cogl3d.org/cogl-reference/): Low-level graphics  
[Mesa](http://mesa3d.org/sourcedocs.html): Low-level graphics  
[harfbuzz](https://www.freedesktop.org/wiki/Software/HarfBuzz/): Low-level font rendering  
[freetype](https://www.freetype.org/freetype2/docs/documentation.html): Low-level font rendering  

#### Functionality
[GTK+ 3](https://developer.gnome.org/gtk3/stable/): UI toolkit  
[gdk-pixbuf](https://developer.gnome.org/gdk-pixbuf/2.36/): Image manipulation  
[GMime](https://developer.gnome.org/gmime/stable/): File format support  
[GStreamer](https://gstreamer.freedesktop.org/documentation/): Low-level multimedia  
[Farstream](https://www.freedesktop.org/wiki/Software/Farstream/#documentation): Communication  
[libxml2](http://www.xmlsoft.org/docs.html): File format support  
[libxslt](http://xmlsoft.org/libxslt/docs.html): File format support  
[SQLite](https://sqlite.org/docs.html): Data storage  
[JSON GLib](https://developer.gnome.org/json-glib/1.2/): File format support  
[Soup](https://developer.gnome.org/libsoup/stable/): Network protocol  
[LLVM](http://llvm.org/docs/): Compiler technology  

---
