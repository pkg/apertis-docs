---
short-description: 'Use the SDK virtual machine for network management and boot'

authors:
    - name: Denis Pynkin
      email: denis.pynkin@collabora.com
      years: [2019]

license: CC-BY-SAv4.0
...

# Setup SDK VM as network management server

## Background

Sometimes it is needed to have own testing environment for the target board.
For example if developer is working in restricted network and want to change some automatic network settings or need to have playground with network bootable system.

For the board network setup, boot and IPAM management we need to provide services below:

* DHCP -- IP addresses management
* DNS -- name server
* NTP -- time settings
* TFTP -- boot over the network support (for netboot only)
* NFS -- network file system (for netboot only)


## Preconditions

* There is already installed SDK VM as described in [installation guide](install.md).

* It is expected what the USB Network Adapter is used as additional network interface dedicated for the development board.

* To avoid any influence from the host and isolate the network connection between the host and development board please attach the USB Network Adapter as USB device -- this allows Apertis SDK to manage the dedicated network:

![](media/vm-usbnet-setup.png)

* The USB Network Adapter is attached as USB device into SDK VM and connected to development board:
![](media/vm-usbnet.png)

* Check the network interfaces after the SDK VM boot:

```
# connmanctl services
*AO Wired                ethernet_525400233200_cable
*AR Wired                ethernet_5254000b00b5_cable
```
Let's assume what USB Network Adapter is shown as service `ethernet_5254000b00b5_cable` and `ens4` interface on SDK VM.

In this document we use dedicated network `192.168.42.0/24` for connection between the SDK VM and development board.

## Assign static address for the USB Network Adapter

Assign address `192.168.42.1` for the `ens4` interface on SDK VM:

```
sudo connmanctl config ethernet_5254000b00b5_cable --ipv4 manual 192.168.42.1 255.255.255.0
```


## Firewall and network forwarding setup

Check the interface name which is dedicated for devices management with command `ip addr show` -- you will see the correct interface name with address `192.168.42.1` (`ens4` in example).

* Add rules allowing to access to DNS, DHCP and NTP services on VM:

```
sudo iptables -D INPUT -j REJECT --reject-with icmp-host-prohibited
sudo iptables -A INPUT -p udp --dport 67 -i ens4 -j ACCEPT
sudo iptables -A INPUT -p udp --dport 53 -i ens4 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 53 -i ens4 -j ACCEPT
sudo iptables -A INPUT -p udp --dport 123 -i ens4 -j ACCEPT
sudo iptables -A INPUT -j REJECT --reject-with icmp-host-prohibited
```

* Allow access to outer network for development board:

```
sudo iptables -D FORWARD -j REJECT --reject-with icmp-host-prohibited
sudo iptables -A FORWARD -i ens4 -j ACCEPT
sudo iptables -A FORWARD -o ens4 -j ACCEPT
sudo iptables -A FORWARD -j REJECT --reject-with icmp-host-prohibited
sudo iptables -t nat -A POSTROUTING --src 192.168.42.0/24 -j MASQUERADE
```

* Save iptables configuration with command:

```
sudo iptables-save | sudo tee /etc/iptables/rules.v4
```

* Allow forwarding

```
sysctl -w net.ipv4.ip_forward=1
```
And add the same into the system's configuration to enable forwarding automatically after reboot:

```
echo "net.ipv4.ip_forward=1" | sudo tee /etc/sysctl.d/enable-forward.conf
```

## Install additional packages

Here we use [dnsmasq](http://www.thekelleys.org.uk/dnsmasq/doc.html) lightweight server to provide DNS, DHCP and TFTP services.
Also we need to provide a correct time with [NTP](http://www.ntp.org/documentation.html) server for development boards without backup battery allowing to use HTTPS/TLS connections and OTA upgrades.

If the proxy server is needed to access to outer network then need to add it into the APT configuration file `/etc/apt/apt.conf.d/90proxy`:
```
Acquire::http::Proxy "http://10.168.128.45:3128";
Acquire::https::Proxy "http://10.168.128.45:3128";
```

Install additional packages on the SDK VM:
```
sudo apt-get update
sudo apt-get install -y dnsmasq ntp
```
## Configure DNS and DHCP

By default `dnsmasq` works as caching DNS service allowing the name resolution for managed devices
Add minimal configuration file `/etc/dnsmasq.d/10-dhcp.conf` for address management:

```
listen-address=192.168.42.1
bind-interfaces
dhcp-range=192.168.42.10,192.168.42.200,10m
dhcp-option=option:ntp-server,192.168.42.1
```

The last option forces to use SDK VM for time synchronization.

## Configure NTP service

Minimal working configuration file `/etc/ntp.conf` for the system without access to external time sources. Omit the drift file since for VM it doesn't help.

```
# By default, exchange time with everybody, but don't allow configuration.
restrict -4 default kod notrap nomodify nopeer noquery limited
restrict -6 default kod notrap nomodify nopeer noquery limited

# Local users may interrogate the ntp server more closely.
restrict 127.0.0.1
restrict -6 ::1

# Use ourselves as single source
server 127.127.1.0 prefer iburst
# Allow divices to use our time server
restrict 192.168.42.0 mask 255.255.255.0 nomodify notrap
```

## Start DHCP/DNS/NTP services

Enable and restart services with new configuration:

```
sudo systemctl enable dnsmasq ntp
sudo systemctl restart dnsmasq ntp
```

## Netboot configuration (TBD)

TFTP and NFS services are needed in addition to allow development boards to boot over the network.

## Hints

* The ethernet USB dongle stops working on the SDK when the target board reboots

  As a workaround, connect both the SDK USB dongle and the target to an ethernet
  **L2** hub/switch to keep the carrier signal stable on the SDK side even when
  the target board reboots.

* I want to attach several boards in the same time

  Use a **L2** hub/switch to connect the SDK and the target boards.

* I have only a L3 switch/router

  If you are using a **L3** router between the SDK and target instead of a **L2** switch/hub,
  make sure it can act as a **L2** switch by disabling services which may affect connectivity
  like the DHCP server on the router. It's also worth disabling any other service which is not
  needed for this use, like wireless access.
