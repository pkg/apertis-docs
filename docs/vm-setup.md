---
short-description: 'Get the SDK virtual machine up and running'

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Setting up the virtual machine

## Installing VirtualBox

Oracle VM VirtualBox is used to run the SDK. If you have not yet installed the current version of this software, please follow these steps:

* Download the most recent version of the VirtualBox installation file [here](https://www.virtualbox.org/wiki/Downloads). For Apertis, we recommend version 5.0.12 or above.

* Run the `Setup.exe`.

* Follow the installation steps to finish installing VirtualBox.

> For instructions on uninstalling VirtualBox, you can take a look at the [user manual](https://www.virtualbox.org/manual/ch02.html) and choose your host system OS from the table of contents.

## Create a virtual machine with the SDK image

* Extract the gzipped VDI image file you [downloaded in the previous step](install.md#download-an-sdk-vm-image) to a local folder on your PC. The image for the virtual machine is a single file.

* Start the VirtualBox application (Oracle VM VirtualBox in the Start
menu).

    ![](media/fig-1.png)

* Go to `Machine ▸ New` or click the **New** Icon. This launches the **Create New Virtual Machine** screen. If you would like additional information about creating a new virtual machine you can find it in the [VirtualBox manual](https://www.virtualbox.org/manual/ch01.html#gui-createvm)

* Enter a name and select **Type: Linux**, **Version: Ubuntu (64 bit)** from the menu.

    ![](media/fig-2.png)

* Select the RAM size. Change the values manually according to your requirements. Assign at least 50% RAM for the virtual machine if your total RAM is more than 2 GB.

    ![](media/fig-3.png)

    > Not enough RAM might cause problems while you use the SDK (the simulator might be too slow, Eclipse might crash).

* Select **Use an existing virtual hard drive file** and browse to the location of your unzipped file.

    ![](media/fig-4.png)

* Click **Create** to create the virtual machine.

* If using an apertis AMD image select **Enable EFI** - by selecting your virtual machine and clicking **Settings**. Select **System-> Motherboard**. If *not* using AMD image this setting is not needed

    ![](media/fig-5.png)

    ![](media/fig-6.png)

* If you want to start your virtual machine from the desktop without having to open the VirtualBox every time, you can create a desktop icon. Right-click the entry of your virtual machine on the left and choose **Create Shortcut on Desktop** from the menu.

    ![](media/fig-9.png)

## Start the virtual machine for the first time

Use your just created desktop shortcut, or click **Start** in VirtualBox to start the virtual machine. The boot-up-process might take a few seconds.

On starting the virtual machine, VirtualBox might display some popup windows informing you about mouse, keyboard and color settings which might be different on the VM. Please read through the messages and click **OK** for all of them. If you check the **Do not show this message again** checkbox, you can permanently disable these popup messages for this virtual machine.

Find more information on the mouse and keyboard settings [here](https://www.virtualbox.org/manual/ch01.html#keyb_mouse_normal).

## Configure the Network Proxy

If you need to configure a network proxy to establish an internet connection, you can make the necessary configuration by clicking **ApplicationsSettingsNetwork proxy** and following the instructions given in the terminal window.

![](media/fig-19.png)

The dialog will ask you for the following information:

  - Proxy Address (Proxy server name : port num)

  - Workstation Name

  - Proxy User

  - NT Domain

  - Proxy Password

## Installing VirtualBox Guest Additions:

It is necessary to install guest additions corresponding to the virtual box version installed. Make sure to backup important data, since updating the Guest Additions might overwrite some of your data.

Start the SDK and go to `Devices ▸ Insert Guest Additions CD Image…` on the VirtualBox menu bar. A virtual device will appear in the file
orer.

![](media/fig-16.png)

![](media/fig-17.png)

The CD image should be shown on the desktop. Now, follow the below instructions:

```
Open the guest additions folder by double clicking on the CD icon
Open a terminal by rightlick->open terminal in the guest additions folder
Run the command - sudo ./VBoxLinuxAdditions.run
```

![](media/fig-18.png)

Once the Guest Additions are installed successfully (the process might take a few minutes), restart the virtual machine, see [here](vm-management.md) if you need help with that.

## Setting up shared folders

Go to **Settings** and click **Shared Folders** and select **Add Shared Folders** (use the icon on the left hand side). Browse to the path you created your share folder in (e.g. `C:\SHARE`). Click **OK** and close the Settings window. The VirtualBox VM Settings can only be edited when the VM is closed. So please close all VMs if any should be running, before setting up a shared folder.

![](media/fig-7.png)

![](media/fig-8.png)

Once you start the virtual machine, go to **ApplicationsTerminal Emulator** and run this command in the terminal:

```
sudo mount –t vboxsf HOST_DIR_NAME GUEST_DIR_NAME
```

E.g:

```
sudo mount –t vboxsf SHARE /mnt
```

This command will mount the share folder to the current `/mnt`.

### Next

|-|
| ![](media/continue-arrow.png) **Ensure that the virtual machine is properly configured.** [Continue on to configuring the virtual machine](vm-config.md) |
