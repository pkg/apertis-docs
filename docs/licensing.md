---
short-description: "A summary of licensing implications"

authors:
    - name: Ekaterina Gerasimova
      email: kat@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Choose a license for your application

You can chose any license that you wish for your application. If you use any of the Apertis libraries, you will still be able to keep your application closed source or open source.

Apertis is a Free software platform. If you wish to follow the licensing model of Apertis, you can find further information in the [platform licensing](https://wiki.apertis.org/Licensing) guide.

It is good practice to include information about copyright and the license at the top of each code file. This is often referred to as the header.

For example, a copyright notice can look like this:
```
   /*
    * Copyright © 2015, 2016 Anita Developer <a.developer@example.com>
    */
```

If you use a Free software license, you will need to follow the guidelines provided by that license. We recommend that you also include the [SPDX license identifier](https://spdx.org/licenses/). For example, the license header for MPL 2.0 would look like this:
```
    /*
     * Copyright © 2015, 2016 Anita Developer <a.developer@example.com>
     *
     * SPDX-License-Identifier: MPL-2.0
     * This Source Code Form is subject to the terms of the Mozilla Public
     * License, v. 2.0. If a copy of the MPL was not distributed with this
     * file, You can obtain one at http://mozilla.org/MPL/2.0/.
     */
```

The license you choose will not affect your ability to [distribute the application](app-distribution.md) through the Apertis app store.
