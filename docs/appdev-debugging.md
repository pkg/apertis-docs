---
short-description: "How to debug an application"
...

# Debugging app in eclipse

Follow these steps to debug an application in Eclipse:

* Install your application to the simulator using **Install app to simulator**.

* Go to `Run ▸ Debug Configurations` (You should be in the C/C++ Perspective)

* Double click on the C/C++ application. It will create a new debug configuration.

* In the main tab, for C/C++ Application entry, press the **Browse** button and select `/Applications/BundleID/bin/app_bin_name`

* In Arguments tab, set app-name to ProjectName , and set working directory to `/Applications/BundleID` using the **File System** option by disabling Use Default Checkbox.

* In Environment tab, Select and check `LD_LIBRARY_PATH`. Suffix /Applications/BundleID as value for `LD_LIBRARY_PATH`

* In source tab. Click on `Add ▸ File System directory` and select `/Applications/BundleID`, then press **OK**.

* Apply all these settings and click on the **Debug** button.

* When asked to switch to the debug perspective, click **Yes**. The default breakpoint is on main(). So press F6 for step by step debugging

# Viewing simulator logs in the eclipse console

When you start the simulator from Eclipse, the logs can be seen in the Eclipse console itself. This helps in debugging and analysing the applications.

![](media/image47.png)

