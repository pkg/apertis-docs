---
short-description: "Basic VirtualBox tips for developers not familiar with that technology"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Managing the virtual machine

## Shutting down / restarting the virtual machine

If you want to shut the virtual machine down, you can use the icon in the top right corner of the screen. If you want to restart it, go to **Applications Menu** and click **Log Out**:

![](media/fig-11.png)

In the dialog box, you can choose if you want to log out, restart the virtual machine, or shut it down.

![](media/fig-12.png)

## Uninstall the virtual machine

To uninstall the virtual machine, open VirtualBox and right click the machine you want to remove. Chose **Remove** from the menu.

![](media/fig-13.png)

In the following dialog you can decide if you want to remove the virtual machine from VirtualBox or if you want to delete the files containing the virtual machine from your hard drive as well. This will remove the hard drive of the virtual machine and all files saved there. Please make sure to make backups of the files you still need before deleting all files. **Remove only** will just delete the virtual machine from VirtualBox but leave the files containing the virtual machine intact.

## Audio settings

In order to make sure that the audio player output is heard from the simulator make sure that the below settings are done:

- Unmute Audio in host machine.

- Go to `Applications menu ▸ Multimedia ▸ Pulse Audio Volume Control`

- Select **Output Devices** tab.

- Make sure that the Audio speaker is **Unmuted**.

With these settings, Audio player output should be heard over speakers.
