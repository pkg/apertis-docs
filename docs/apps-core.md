---
short-description: "Generic system applications"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# Core applications

These are apps that are bundled together with the system and get updated
when system gets updated:

  - rhayader – Browser

  - frampton – Audio player

  - eye – video player
