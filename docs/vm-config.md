---
short-description: 'Basic customization of the SDK virtual machine'

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Configuring the virtual machine

## Change the keyboard layout

Once the SDK is installed, you might need to modify your keyboard layout for the Ubuntu operating system. The default keyboard layout is **US**. To change it, please follow these steps:

* In the Ubuntu system, go to `Applications Menu ▸ Settings` and choose **Keyboard**.

* In the dialog box, select the tab **Layout**, uncheck **Use system defaults** and click **Edit** under Keyboard layout. Select your required keyboard layout from the list, click **OK** and close the window. You do not need to select a Keyboard model.

    ![](media/fig-10.png)

## Adjusting virtual machine window size

Once the guest additions are installed, the window size can be changed. Select the option **Adjust Window Size**. Then resize the window to make it appear as a normal working size.

![](media/fig-15.png)

## Put the virtual machine in Fullscreen mode

You can switch the display of the SDK to fullscreen mode by selecting **View** in the VirtualBox menu and choosing **Switch to Fullscreen**.

![](media/fig-15.png)

You can still access the most important options of VirtualBox in the menu that appears at the bottom of the screen when you get close to it with your cursor.

### Tips for managing the virtual machine

|-|
| **If you need help restarting or uninstalling the virtual machine,** [read about how to manage the virtual machine](vm-management.md) |

### Next

|-|
| ![](media/continue-arrow.png) **Let's get started using the SDK.** [Continue on to using the SDK](sdk-usage.md) |
